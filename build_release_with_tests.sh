#!/bin/bash

mkdir build
pushd build
cmake .. -DBUILD_AND_RUN_TESTS=ON -DCMAKE_BUILD_TYPE=Release
cmake --build .
popd
ln -s ./build/compile_commands.json .
