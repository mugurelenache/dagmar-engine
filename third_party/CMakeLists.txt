set(GLFW_BUILD_EXAMPLES OFF CACHE INTERNAL "Build the GLFW example programs")
set(GLFW_BUILD_TESTS OFF CACHE INTERNAL "Build the GLFW test programs")
set(GLFW_BUILD_DOCS OFF CACHE INTERNAL "Build the GLFW documentation")
set(GLFW_INSTALL OFF CACHE INTERNAL "Generate installation target")
set(JSON_BuildTests OFF CACHE INTERNAL "")

add_subdirectory(json)
add_subdirectory(glfw)
add_subdirectory(glm)
add_subdirectory(tinyobjloader)
if(${BUILD_AND_RUN_TESTS})
	add_subdirectory(Catch2)
endif()
add_subdirectory(glew-cmake)

add_library(glew::glew ALIAS libglew_static)
add_library(glfw::glfw ALIAS glfw)
add_library(tinyobjloader::tinyobjloader ALIAS tinyobjloader)

include(imgui.cmake)
include(stb.cmake)

set(CATCH_DIR ${CMAKE_CURRENT_SOURCE_DIR}/Catch2 PARENT_SCOPE)