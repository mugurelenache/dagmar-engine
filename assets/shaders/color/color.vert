#version 460 core
#extension GL_ARB_explicit_uniform_location : enable

// Input
layout(location = 0) in vec3 aPos;

// Uniforms
layout(location = 0) uniform mat4 projection;
layout(location = 1) uniform mat4 view;
layout(location = 2) uniform mat4 model;
layout(location = 3) uniform vec3 cameraPosition;

// Output

void main()
{
    vec4 outPos = projection * view * model * vec4(aPos.xyz, 1.0);

    gl_Position = outPos;
}