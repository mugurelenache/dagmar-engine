#version 460
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_explicit_uniform_location : enable

/////////////// STRUCTS ///////////////////////
struct MATERIAL
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 emissive;
    float specularExponent;
};
///////////////////////////////////////////////

///////////////// INPUTS //////////////////////
layout(location = 0) in vec3 inFragPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inLightPos;
layout(location = 3) in vec3 inCameraPos;
// layout(location = 4) in vec4 inColor;
// layout(location = 5) in vec2 inUV;
///////////////////////////////////////////////

/////////////////// UNIFORMS //////////////////
// layout(location = 0) uniform Matrial material;
///////////////////////////////////////////////

////////////////////// OUTPUTS ////////////////
layout(location = 0) out vec4 outFBColor;
///////////////////////////////////////////////


void main()
{
    // vec3 color = inColor;
    //     color *= texture(textureSampler, inUV).rgb;

    vec3 fragPos = inFragPos.xyz;
    vec3 halfVec = normalize(inLightPos + inCameraPos);
    

    // Ambient computation
    vec3 constant_ambient = vec3(0.3f, 0.3f, 0.3f);
    vec3 ka = vec3(0.3f);
    // vec3 ambient = ka * material.ambient.xyz * color;
    vec3 ambient = ka * constant_ambient.xyz;


    // Diffuse computation
    vec3 constant_diffuse = vec3(0.8f, 0.3f, 0.2f);
    float kd = max(dot(inLightPos, inNormal), 0.0f);
    vec3 diffuse = kd * constant_diffuse;
    // vec3 diffuse = kd * material.diffuse.xyz;


    // Specular computation
    // float ks = pow(max(dot(inNormal, halfVec), 0.0), material.specularExponent);
    // vec3 specular = ks * material.specular.xyz;
    vec3 constant_specular = vec3(0.5f, 0.5f, 0.5f);
    float constant_spec_exponent = 16.0f;
    float ks = pow(max(dot(inNormal, halfVec), 0.0), constant_spec_exponent);
    vec3 specular = ks * constant_specular.xyz;

    // Outputting color
    float constant_alpha = 1.0f;
    outFBColor = vec4(ambient + diffuse + specular, constant_alpha);
    // outFBColor = vec4(ambient + diffuse + specular, material.diffuse.a);
}