set(ASSETS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/assets)
set(ASSETS_INSTALL_DIR ${CMAKE_INSTALL_PREFIX}/assets)
set(ASSETS_BUILD_DIR ${CMAKE_CURRENT_BINARY_DIR}/assets)

string(FIND ${CMAKE_GENERATOR} "Visual Studio" VS_GENERATOR)

if(NOT (${VS_GENERATOR} STREQUAL "-1"))
    set(ASSETS_FROM_MAIN "../../assets/")
else()
    set(ASSETS_FROM_MAIN "../assets/")
endif()

add_custom_target(assets_targ ALL
	COMMENT "Copying assets dir"
	COMMAND ${CMAKE_COMMAND} -E make_directory ${ASSETS_INSTALL_DIR} ${ASSETS_BUILD_DIR}
	COMMAND ${CMAKE_COMMAND} -E copy_directory ${ASSETS_DIR} ${ASSETS_INSTALL_DIR}
	COMMAND ${CMAKE_COMMAND} -E copy_directory ${ASSETS_DIR} ${ASSETS_BUILD_DIR}
)
