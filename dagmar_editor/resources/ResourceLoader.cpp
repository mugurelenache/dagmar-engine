#include "dagmar/ResourceLoader.h"

#include <string>
#include <vector>

#include "dagmar/ResourceManager.h"
#include "dagmar/FileSystem.h"
#include "dagmar/Coordinator.h"
#include "dagmar/RenderingSystem.h"

/**
 * @brief Loads the internal assets: textures, meshes, etc.
 */
void dag::ResourceLoader::loadInternalAssets()
{
    auto internals = dag::filesystem::getAssetsPath() / "internal" / "default_meshes" / "dat";

    std::vector<std::string> internalsDat = {
        "Cube.dat",
        "Sphere.dat",
        "Starry.dat",
        "Torus Knot.dat",
        "Cylinder.dat",
        "Plane.dat",
        "Teapot.dat",
        "Tetrahedra.dat",
        "Torus.dat"};


	for (auto& internalPath : internalsDat)
	{
        resourceManager->readBin(internals / internalPath);
        dag::Mesh mesh;
        resourceManager->getMesh(0, mesh);


        std::vector<dag::VertexAttribute> atts = {
            dag::VertexAttribute(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0),
            dag::VertexAttribute(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)))};

        
        auto rs = dag::coordinator->getSystem<dag::RenderingSystem>();
        rs->vaos.push_back(mesh.generateVAO(atts));
        std::string name = internalPath.substr(0, internalPath.length() - 4);
        rs->meshRenderers.push_back(dag::MeshRenderer{.name = name, .vaoID = rs->vaos.size() - 1});
	}
}
