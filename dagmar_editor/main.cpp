// Include standard headers
#include <cstdio>
#include <cstdlib>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <string>
#include <vector>
#include <random>

using namespace glm;

#define TINYOBJLOADER_IMPLEMENTATION
#include "dagmar/Components.h"
#include "dagmar/Coordinator.h"
#include "dagmar/FileSystem.h"
#include "dagmar/KeyManager.h"
#include "dagmar/Log.h"
#include "dagmar/Mesh.h"
#include "dagmar/ModuleLifecycle.h"
#include "dagmar/PhysicsSystem.h"
#include "dagmar/Renderer.h"
#include "dagmar/RenderingSystem.h"
#include "dagmar/ResourceManager.h"
#include "dagmar/ResourceLoader.h"
#include "dagmar/VertexArrayObject.h"
#include "dagmar/WindowManager.h"
#include "dagmar/TimeManager.h"
#include "dagmar/EngineConfig.h"
#include "dagmar/Vertex.h"
#include "dagmar/Scene.h"
#include "dagmar/UIManager.h"

int main(void)
{
    dag::ModuleLifecycle::get().startup();

    auto& ecsCoord = dag::coordinator;
    auto renderingSystem = ecsCoord->getSystem<dag::RenderingSystem>();
    auto physicsSystem = ecsCoord->registerSystem<dag::PhysicsSystem>();
    physicsSystem->coordinator = ecsCoord;
    physicsSystem->startup();

	// Load the other VAOs
    {
        dag::resourceLoader->loadInternalAssets();
    }

    dag::scene->coordinator = ecsCoord;
    ecsCoord->registerComponent<dag::Name>();

    dag::scene->createEntity("First entity");
    dag::scene->createEntity("Second entity");
    dag::scene->createEntity("Third entity");

    int i = 0;
    for (auto& entity : dag::scene->entities)
    {
        if (i == 0)
        {
            ecsCoord->addComponent(entity, dag::RigidBody{ .impulse = glm::vec3(20.0f, 0.0f, 0.0f), .mass = 0.05f });
            ecsCoord->addComponent(entity, dag::Collision{ .radius = 0.1f, .center = glm::vec3(0.0f) });
        }
        else
        {
            ecsCoord->addComponent(entity, dag::RigidBody{ .impulse = glm::vec3(-20.0f, 0.0f, 0.0f), .mass = 0.049f });
            ecsCoord->addComponent(entity, dag::Collision{ .radius = 0.1f, .center = glm::vec3(0.0f) });
        }
        ++i;
    }

    std::shared_ptr<dag::UIManager> engineUIManager = std::make_shared<dag::UIManager>();

    dag::scene->serialize("output_graph.json");
    //dag::scene->deserialize("output_graph.json");

    // Check if the ESC key was pressed or the window was closed
    while (glfwGetKey(dag::windowManager->window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
           !glfwWindowShouldClose(dag::windowManager->window))
    {
        dag::timeManager->updateFrameStart();

        glfwPollEvents();

        for (auto& entity : dag::scene->entities)
        {
            auto& descriptorSet = ecsCoord->getComponent<dag::DescriptorSet>(entity);
            auto& transform = ecsCoord->getComponent<dag::Transform>(entity);
            transform.updateModelMatrix();
            descriptorSet.descriptors[0].data = ecsCoord->getComponent<dag::Transform>(entity).modelMatrix;
        }

        dag::renderer->draw();

        engineUIManager->drawMainUI(dag::renderer->compositeFramebuffer.getColorAttachment(0));
        glfwSwapBuffers(dag::windowManager->window);
    }

    dag::ModuleLifecycle::get().shutdown();

    return 0;
}
