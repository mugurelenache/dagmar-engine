#include "dagmar/PlatformDetector.h"

#ifdef _WIN32
#include <windows.h>
#endif

/**
 * @brief Get name for the module
 */
std::string dag::PlatformDetector::getName() const
{
    return "PlatformDetector";
}

/**
 * @brief Detects the platform the project is running on
 */
void dag::PlatformDetector::detect()
{
#ifdef __linux__
    platform = Platform::Linux;
#elif _WIN32
    platform = Platform::Windows;
#endif
}

/**
 * @brief Activate platform specific features
 */
void dag::PlatformDetector::activatePlatformFeatures()
{
#ifdef __linux__
    // We're happy with linux
    // Currently no commands have been necessary
#elif _WIN32
    // We're happy with Windows
    // Currently no commands have been necessary
#endif
}

/**
 * @brief Startup class functionality 
 */
void dag::PlatformDetector::startup()
{
    detect();
    activatePlatformFeatures();
}

/**
 * @brief Perform Shutdown 
 */
void dag::PlatformDetector::shutdown()
{
}

