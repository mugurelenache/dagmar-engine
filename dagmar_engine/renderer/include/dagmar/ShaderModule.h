#pragma once

#include <filesystem>

namespace dag
{
    /**
     * @brief Shader module class
     */
    class ShaderModule
    {
      public:
        unsigned int id;
        int type;

      public:
        ShaderModule() = default;
        ShaderModule(std::filesystem::path const& shaderPath, int const& shaderType);

        void destroy();
        void setCodeAndCompile(std::filesystem::path const& shaderPath, int const& shaderType);

        bool operator==(ShaderModule const& other) const;
        bool operator!=(ShaderModule const& other) const;
    };
} // namespace dag
