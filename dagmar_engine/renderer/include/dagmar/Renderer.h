#pragma once
#include "dagmar/Framebuffer.h"
#include "dagmar/Mesh.h"
#include "dagmar/Module.h"
#include "dagmar/RenderingSystem.h"

#include <memory>

#include "GLFW/glfw3.h"

namespace dag
{
    /**
    * @brief Renderer currently handles the relationship between the framebuffer we draw our game to,
    * the drawing of that frame buffer to the window, and the drawing of UI elements
    */

    class Renderer : public Module
    {
      public:
        dag::Mesh screenMesh;
        dag::ShaderProgram screenShader;
        dag::VertexArrayObject<float, uint32_t> screenMeshVAO;

        std::shared_ptr<dag::RenderingSystem> renderingSystem;

        Framebuffer compositeFramebuffer;

      public:
        // default because of Module
        Renderer() = default;
        // default because of Module
        ~Renderer() = default;

        // instanciate our framebuffer to draw to, it's attachments, and our texture.
        void startup() override;

        // clean up shaders, framebuffer, render buffer, etc.
        void shutdown() override;

        // get the name of the class for Module
        std::string getName() const override;

        // main draw call. This is the entry point for drawing the whole application.
        void draw();

        void windowSizeCallback(GLFWwindow* window, int width, int height);
    };

    inline std::shared_ptr<Renderer> renderer(new Renderer());
} // namespace dag