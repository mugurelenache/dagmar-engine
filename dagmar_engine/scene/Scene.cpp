#include "dagmar/Scene.h"
#include <nlohmann/json.hpp>
#include <fstream>
#include <iostream>

using json = nlohmann::json;

dag::Scene::Scene(){
    currentEntity = -1;
}

void dag::Scene::createEntity(std::string name){
    
    entities.push_back(coordinator->createEntity());
    auto& entity = entities[entities.size() - 1];
    auto defaultMesh = coordinator->getSystem<RenderingSystem>()->meshRenderers[0];

    coordinator->addComponent(entity, dag::Name{name});
    coordinator->addComponent(entity, dag::Transform{});
    coordinator->addComponent(entity, defaultMesh);
    
    coordinator->getComponent<dag::Transform>(entity).updateModelMatrix();

    dag::DescriptorSet descriptorSet;

    // IDs in the vector rather than their GL ID
    descriptorSet.shaderProgramID = 0;

    dag::Descriptor descriptorModel;
    descriptorModel.fID = dag::RenderingSystem::UniformFuncs::UniformMatrix4fv;
    descriptorModel.name = "model";
    descriptorModel.data = coordinator->getComponent<dag::Transform>(entity).modelMatrix;

    descriptorSet.descriptors.push_back(descriptorModel);
    coordinator->addComponent(entity, descriptorSet);
    currentEntity = entities.size() - 1;
    
    recomputeSceneGraph();
}

void dag::Scene::destroyEntity(dag::Entity const& entity){
    coordinator->destroyEntity(entity);

    for(size_t i = 0; i < entities.size() - 1; ++i){
        if(entity == entities[i]){
            for(size_t j = i; j < entities.size() - 1; ++j){
                entities[j] = entities[j+1];
            }
        break;
        }
    }
    entities.erase(entities.end() - 1);

    recomputeSceneGraph();
}

void dag::Scene::startup(){

}

void dag::Scene::shutdown(){

}

std::string dag::Scene::getName() const {
    return "Scene";
}


void dag::Scene::recomputeSceneGraph(){
    // clear before
    sceneGraph.resize(entities.size());
    int i = 0;
    for(auto const& entity : entities){
        auto nameComponent = coordinator->getComponent<dag::Name>(entity);
        sceneGraph[i] = new char[nameComponent.name.size() + 1];
        std::strcpy (sceneGraph[i++], nameComponent.name.c_str());
    }
}

void dag::Scene::serialize(std::filesystem::path const & output_path)
{
    json big_j;

    json j;

    auto entityArray = json::array();

    for (dag::Entity const& e : entities)
    {
        j.clear();
        j["name"] = coordinator->getComponent<Name>(e).name;

        if (coordinator->hasComponent<Transform>(e))
        {
            auto& transform = coordinator->getComponent<Transform>(e);
            j["transform"]["position"]["x"] = transform.position.x;
            j["transform"]["position"]["y"] = transform.position.y;
            j["transform"]["position"]["z"] = transform.position.z;
            j["transform"]["rotation"]["x"] = transform.rotation.x;
            j["transform"]["rotation"]["y"] = transform.rotation.y;
            j["transform"]["rotation"]["z"] = transform.rotation.z;
            j["transform"]["scale"]["x"] = transform.scale.x;
            j["transform"]["scale"]["y"] = transform.scale.y;
            j["transform"]["scale"]["z"] = transform.scale.z;
        }

        if (coordinator->hasComponent<RigidBody>(e))
        {
            j["rigidbody"]["mass"] = coordinator->getComponent<RigidBody>(e).mass;
            j["rigidbody"]["velocity"]["x"] = coordinator->getComponent<RigidBody>(e).velocity.x;
            j["rigidbody"]["velocity"]["y"] = coordinator->getComponent<RigidBody>(e).velocity.y;
            j["rigidbody"]["velocity"]["z"] = coordinator->getComponent<RigidBody>(e).velocity.z;
            j["rigidbody"]["acceleration"]["x"] = coordinator->getComponent<RigidBody>(e).acceleration.x;
            j["rigidbody"]["acceleration"]["y"] = coordinator->getComponent<RigidBody>(e).acceleration.y;
            j["rigidbody"]["acceleration"]["z"] = coordinator->getComponent<RigidBody>(e).acceleration.z;
            j["rigidbody"]["netForce"]["x"] = coordinator->getComponent<RigidBody>(e).netForce.x;
            j["rigidbody"]["netForce"]["y"] = coordinator->getComponent<RigidBody>(e).netForce.y;
            j["rigidbody"]["netForce"]["z"] = coordinator->getComponent<RigidBody>(e).netForce.z;
            j["rigidbody"]["externalForce"]["x"] = coordinator->getComponent<RigidBody>(e).externalForce.x;
            j["rigidbody"]["externalForce"]["y"] = coordinator->getComponent<RigidBody>(e).externalForce.y;
            j["rigidbody"]["externalForce"]["z"] = coordinator->getComponent<RigidBody>(e).externalForce.z;
            j["rigidbody"]["impulse"]["x"] = coordinator->getComponent<RigidBody>(e).impulse.x;
            j["rigidbody"]["impulse"]["y"] = coordinator->getComponent<RigidBody>(e).impulse.y;
            j["rigidbody"]["impulse"]["z"] = coordinator->getComponent<RigidBody>(e).impulse.z;
        }

        if (coordinator->hasComponent<Collision>(e))
        {
            j["collision"]["radius"] = coordinator->getComponent<Collision>(e).radius;
            j["collision"]["center"]["x"] = coordinator->getComponent<Collision>(e).center.x;
            j["collision"]["center"]["y"] = coordinator->getComponent<Collision>(e).center.y;
            j["collision"]["center"]["z"] = coordinator->getComponent<Collision>(e).center.z;
        }
        entityArray.push_back(j);
    }

    big_j["entities"] = entityArray;

    std::ofstream output_file_stream(output_path.generic_string());

    output_file_stream << std::setw(4) << big_j << std::endl;

    output_file_stream.close();
}

void dag::Scene::deserialize(std::filesystem::path const& input_path)
{
    std::ifstream i(input_path);
    json j;
    i >> j;

    deserialize(j);
}

void dag::Scene::deserialize(json& input_json)
{
    for (auto entity : input_json["entities"])
    {
        createEntity(entity["name"]);
        // even easier with structured bindings (C++17)
        for (auto& [key, value] : entity.items()) {
            std::cout << key << " : " << value << "\n";

            if (key == "collision")
            {
                coordinator->addComponent(entities.back(), dag::Collision{ .radius = value["radius"], .center = glm::vec3(value["center"]["x"], value["center"]["y"], value["center"]["z"]) });
            }
            else if (key == "rigidbody")
            {
                coordinator->addComponent(entities.back(),
                    dag::RigidBody{ .impulse = glm::vec3(value["impulse"]["x"], value["impulse"]["y"], value["impulse"]["z"]), .mass = value["mass"] });
            }
            else if (key == "transform")
            {
                dag::Transform& t = coordinator->getComponent<dag::Transform>(entities.back());
                t.position = glm::vec3(value["position"]["x"], value["position"]["y"], value["position"]["z"]);
                t.rotation = glm::vec3(value["rotation"]["x"], value["rotation"]["y"], value["rotation"]["z"]);
                t.scale = glm::vec3(value["scale"]["x"], value["scale"]["y"], value["scale"]["z"]);
                t.updateModelMatrix();
            }
        }
    }
}
