#include "dagmar/PhysicsSystem.h"
#include "dagmar/Log.h"

#include <thread>
#include <chrono>

/**
 * @brief Default cosntructor to initialise the global variables & coefficients
  */
dag::PhysicsSystem::PhysicsSystem()
{
    gravity = glm::vec3(0.0f, -9.8f, 0.0f);
    airResistance = 0.0f;
    epsilon = 0.0001f;
    dispersionCoefficient = 0.7f;
    plasticCoefficient = 1.0f;
    timeMultiplier = 1.0f;
}

dag::PhysicsSystem::~PhysicsSystem()
{
    physicsThread.join();
}

/**
 * @brief Register the needed components and the system
  */

void dag::PhysicsSystem::startup()
{
    coordinator->registerComponent<dag::RigidBody>();
    coordinator->registerComponent<dag::Collision>();

    dag::Signature physicsSystemSignature;
    physicsSystemSignature.set(coordinator->getComponentType<dag::Transform>());
    physicsSystemSignature.set(coordinator->getComponentType<dag::RigidBody>());
    physicsSystemSignature.set(coordinator->getComponentType<dag::Collision>());

    coordinator->setSystemSignature<dag::PhysicsSystem>(physicsSystemSignature);

    physicsThread = std::thread([&](void) {
        auto lastRun = std::chrono::high_resolution_clock::now();

        double timeSinceLastRun;
        while (!coordinator->isShuttingDown())
        {
            {
                std::lock_guard<std::mutex> lock(this->physicsMutex);
                timeSinceLastRun = std::chrono::duration<double, std::ratio<1>>(std::chrono::high_resolution_clock::now() - lastRun).count();
                if (coordinator->isShuttingDown())
                {
                    break;
                }
                this->update((timeSinceLastRun / 10.0) * timeMultiplier);
                lastRun = std::chrono::high_resolution_clock::now();
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(1000 / 120));
        }
    });
}

/**
 * @brief Collision checking function for bounding spheres
  */
void dag::PhysicsSystem::checkForCollisions()
{
    // loop through all the unique pairs of entities
    for (auto i = entities.begin(); i != entities.end(); ++i)
    {
        // get the relevant data for the entity
        auto& collisionSphere = coordinator->getComponent<dag::Collision>(*i);
        auto& transform = coordinator->getComponent<dag::Transform>(*i);
        float radius = collisionSphere.radius * std::max(std::max(transform.scale.x, transform.scale.y), transform.scale.z);
        auto& rigidBody = coordinator->getComponent<dag::RigidBody>(*i);
        glm::vec3 centerInWorld = transform.modelMatrix * glm::vec4(collisionSphere.center, 1.0f);

        for (auto j = i; ++j != entities.end();)
        {
            auto& otherCollisionSphere = coordinator->getComponent<dag::Collision>(*j);
            auto& otherTransform = coordinator->getComponent<dag::Transform>(*j);
            glm::vec3 otherCenterInWorld = otherTransform.modelMatrix * glm::vec4(otherCollisionSphere.center, 1.0f);
            float otherRadius = otherCollisionSphere.radius * std::max(std::max(otherTransform.scale.x, otherTransform.scale.y), otherTransform.scale.z);

            // if the objects are closer than the sum of their radii, there is a collision
            if (glm::length(centerInWorld - otherCenterInWorld) < radius + otherRadius - epsilon)
            {
                auto& otherRigidBody = coordinator->getComponent<dag::RigidBody>(*j);
                // compute the collision direction
                if(glm::length(centerInWorld - otherCenterInWorld) < 0.01){
                    break;
                }
                glm::vec3 toVector = glm::normalize(otherCenterInWorld - centerInWorld);
                // compute the midpoint between the objects
                glm::vec3 mid = (centerInWorld + otherCenterInWorld) * 0.5f;
                // update the positions as 'touching', no collision
                transform.position = mid + (collisionSphere.radius + epsilon) * glm::normalize(centerInWorld - mid);
                otherTransform.position = mid + (otherCollisionSphere.radius + epsilon) * glm::normalize(otherCenterInWorld - mid);
                // reflect the forces that move towards the other object and multiply by the plastic coefficient
                rigidBody.externalForce = glm::reflect(rigidBody.externalForce, toVector) * plasticCoefficient;
                otherRigidBody.externalForce = glm::reflect(otherRigidBody.externalForce, toVector) * plasticCoefficient;
                // update the velocity and disperse it if wanted
                rigidBody.velocity = glm::reflect(rigidBody.velocity, toVector) * plasticCoefficient * dispersionCoefficient;
                otherRigidBody.velocity = glm::reflect(otherRigidBody.velocity, -toVector) * plasticCoefficient * dispersionCoefficient;
            }
        }
    }
}

/**
 * @brief Advance all the physics entities by timestep dt
  */
void dag::PhysicsSystem::update(float const& dt)
{
    for (auto const& entity : entities)
    {
        // get relevant data about entity
        auto& transform = coordinator->getComponent<dag::Transform>(entity);

        auto& rigidBody = coordinator->getComponent<dag::RigidBody>(entity);
        
        // euler to move the system forward
        transform.position = transform.position + rigidBody.velocity * dt;

        rigidBody.netForce = rigidBody.impulse + rigidBody.externalForce + airResistance * rigidBody.velocity + gravity;

        rigidBody.acceleration = rigidBody.netForce * (1.0f / rigidBody.mass);

        rigidBody.velocity = rigidBody.velocity + rigidBody.acceleration * dt;

    }
    // check for any collisions and update accordingly
    checkForCollisions();
}