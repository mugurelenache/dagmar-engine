add_library(systems STATIC
    PhysicsSystem.cpp
    RenderingSystem.cpp
)

target_include_directories(systems PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)

install(TARGETS systems)

target_link_libraries(systems PUBLIC 
    glew::glew
    glm::glm
    Dagmar::Components
    Dagmar::ECS
    Dagmar::Renderer
    Dagmar::Graphics
)

add_library(Dagmar::Systems ALIAS systems)
