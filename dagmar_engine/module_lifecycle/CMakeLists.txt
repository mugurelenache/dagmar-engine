add_library(module_lifecycle STATIC
    "ModuleLifecycle.cpp"
)

target_include_directories(module_lifecycle PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(module_lifecycle
    Dagmar::ECS
    Dagmar::PlatformDetector
    Dagmar::Log
    Dagmar::Resources
    Dagmar::EngineConfig
    Dagmar::UI
    Dagmar::Renderer
    Dagmar::Time

)

add_library(Dagmar::ModuleLifecycle ALIAS module_lifecycle)
