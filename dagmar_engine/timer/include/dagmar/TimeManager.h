#pragma once
#include "dagmar/Module.h"

#include <chrono>
#include <memory>

namespace dag
{
    /**
    * @brief Module to keep track of globally useful time
    *
    */
    class TimeManager : public Module
    {
      private:
        std::chrono::high_resolution_clock steadyClock;
        std::chrono::time_point<std::chrono::high_resolution_clock> systemStart;
        std::chrono::time_point<std::chrono::high_resolution_clock> frameStart;

      public:
        // default for Module 
        TimeManager() = default;
        ~TimeManager() = default;

        template <typename T, typename I>
        T timeSinceSystemStart()
        {
            return std::chrono::duration<T, I>(steadyClock.now() - systemStart).count();
        }

        template <typename T, typename I>
        T timeSinceFrameStart()
        {
            return std::chrono::duration<T, I>(steadyClock.now() - frameStart).count();
        }

        void updateFrameStart()
        {
            frameStart = steadyClock.now();
        }

        void startup() override;
        void shutdown() override;

        std::string getName() const override
        {
            return "TimeManager";
        }
    };

    inline std::shared_ptr<TimeManager> timeManager(new TimeManager());
}