#pragma once

#include "Entity.h"
#include <set>

namespace dag
{
    /**
     * @brief Simple system class
     */
    class System
    {
      public:
        virtual ~System() = default;

        std::set<Entity> entities;
    };
} // namespace dag
