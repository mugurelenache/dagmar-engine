#pragma once
#include <bitset>
#include <cstdint>

namespace dag
{
    using ComponentType = std::uint8_t;
    // Maximum types of components
    const ComponentType MAX_COMPONENTS = 32;

    using Signature = std::bitset<MAX_COMPONENTS>;
} // namespace dag
