#pragma once
#include <memory>
#include <filesystem>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include "dagmar/RenderingSystem.h"

namespace dag
{
    /**
     * @brief UIManager defines ImGUI elements
     *
     */
    class UIManager
    {
      private:

        bool renderWindowFocused = false;

    	std::shared_ptr<dag::RenderingSystem> renderingSystem;

      public:
        ImVec2 viewportSize = ImVec2(0, 0);

      public:
        UIManager()
        {
            renderingSystem = coordinator->getSystem<RenderingSystem>();
        }
        ~UIManager() = default;

        // draws the whole UI
        void drawMainUI(unsigned int texColorBuffer);
    };
} // namespace dag