#include "dagmar/UIManager.h"
#include "dagmar/WindowManager.h"

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include "dagmar/Renderer.h"

/**
 * @brief Draws the entire UI
 *
 */
void dag::UIManager::drawMainUI(unsigned int texColorBuffer)
{
    // Backbuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, windowManager->width, windowManager->height);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderer->screenShader.bind();
    renderer->screenMeshVAO.bind();
    glBindTexture(GL_TEXTURE_2D, renderer->compositeFramebuffer.getColorAttachment(0));
    renderer->screenMeshVAO.draw();
    renderer->screenMeshVAO.unbind();
    renderer->screenShader.unbind();
    glFlush();
}